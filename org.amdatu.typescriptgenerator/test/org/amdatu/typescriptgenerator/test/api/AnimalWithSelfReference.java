package org.amdatu.typescriptgenerator.test.api;

import java.util.List;

public class AnimalWithSelfReference {
    public String name;
    public List<AnimalWithSelfReference> childeren;
}
