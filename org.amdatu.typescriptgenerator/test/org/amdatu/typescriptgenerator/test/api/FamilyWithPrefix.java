package org.amdatu.typescriptgenerator.test.api;

import java.util.List;

public class FamilyWithPrefix {
    private String m_name;
    private Dog m_dog;
    private List<Dog> m_dogs;

    public String getName() {
        return m_name;
    }

    public void setName(String name) {
        m_name = name;
    }

    public Dog getDog() {
        return m_dog;
    }

    public void setDog(Dog dog) {
        m_dog = dog;
    }

    public List<Dog> getDogs() {
        return m_dogs;
    }

    public void setDogs(List<Dog> dogs) {
        m_dogs = dogs;
    }

}
