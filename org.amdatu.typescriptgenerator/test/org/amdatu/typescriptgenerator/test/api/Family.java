package org.amdatu.typescriptgenerator.test.api;

import java.util.List;

public class Family {
    public String name;
    public Dog dog;
    public List<Dog> dogs;
}
