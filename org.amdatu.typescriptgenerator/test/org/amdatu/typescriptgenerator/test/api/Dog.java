package org.amdatu.typescriptgenerator.test.api;

public class Dog extends Animal {
    private int serial;

    public int getSerial() {
        return serial;
    }

    public void setSerial(int serial) {
        this.serial = serial;
    }
    
}
