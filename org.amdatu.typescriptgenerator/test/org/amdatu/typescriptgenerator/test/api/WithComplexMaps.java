package org.amdatu.typescriptgenerator.test.api;

import java.util.List;
import java.util.Map;

public class WithComplexMaps {
    public Map<String, List<Dog>> strings;
    public Map<String, List<List<Dog>>> unknown;
}
