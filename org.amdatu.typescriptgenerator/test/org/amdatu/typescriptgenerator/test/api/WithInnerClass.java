package org.amdatu.typescriptgenerator.test.api;

public interface WithInnerClass {

    class Inner {
        public String name;
    }
    
    class WithReferenceToInner {
        public Inner inner;
    }
}
