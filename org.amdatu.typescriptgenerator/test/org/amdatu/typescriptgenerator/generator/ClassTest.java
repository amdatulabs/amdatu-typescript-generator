package org.amdatu.typescriptgenerator.generator;

import static org.amdatu.typescriptgenerator.generator.TestHelper.TEST_JAR_PACAKGE;
import static org.amdatu.typescriptgenerator.generator.TestHelper.TEST_PACAKGE;
import static org.amdatu.typescriptgenerator.generator.TestHelper.createTestSettings;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Scanner;

import org.junit.Before;
import org.junit.Test;

public class ClassTest {
    
    private static final String TEST_DEP_API = "org.amdatu.typescriptgenerator.test.dep.api";
    private Settings m_settings;
    
    @Before
    public void setUp() {
        m_settings = createTestSettings();
        new File(m_settings.output).mkdirs();
    }

    @Test
    public void testAnimalClassToInterface() throws Exception {
        m_settings.classType = Settings.INTERFACE_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Animal.d.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain class name", content.contains("interface Animal {"));
            assertTrue(content + "\nShould contain name field", content.contains("name?: string;"));
            assertFalse(content + "\nShould contain not the export", content.contains("export"));
        }
        
        assertTrue(generator.didWork());
    }

    @Test
    public void testAnimalWithSelfReferenceClassToInterface() throws Exception {
        m_settings.classType = Settings.INTERFACE_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/AnimalWithSelfReference.d.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name?: string;"));
            assertTrue(content + "\nShould contain the list field", content.contains("AnimalWithSelfReference[]"));
            assertFalse(content + "\nShould not contain an import to it self", content.contains("AnimalWithSelfReference.d.ts"));
        }
        
        assertTrue(generator.didWork());
    }

    @Test
    public void testAnimalClassToClass() throws Exception {
        m_settings.classType = Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Animal.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain class name", content.contains("class Animal {"));
            assertTrue(content + "\nShould contain name field", content.contains("name: string;"));
            assertTrue(content + "\nShould contain the export", content.contains("export"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testDogClass() throws Exception {
        m_settings.classType = Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Dog.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name: string;"));
            assertTrue(content + "\nShould contain serial field", content.contains("serial: number;"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testWithReferenceToEnumClass() throws Exception {
        m_settings.classType = Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/WithReferenceToEnum.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("abc: ABC;"));
        }
        
        assertTrue(generator.didWork());
    }

    @Test
    public void testWithReferenceToEnumInterface() throws Exception {
        m_settings.classType = Settings.INTERFACE_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/WithReferenceToEnum.d.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("abc?: any;"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testExtendsBaseClassWithoutExtraPath() throws Exception {
        setSettingsForTestJar();
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_JAR_PACAKGE + "/ExtendsBase.ts");
        assertFalse(outputFile + " should be not be generated as it is skipped: ", outputFile.exists());
        assertFalse(generator.didWork());
    }

    @Test
    public void testExtendsBaseClassWithExtraPath() throws Exception {
        setSettingsForTestJar();
        m_settings.classPath.add("../org.amdatu.typescriptgenerator.testjar/testjar.jar");
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_JAR_PACAKGE + "/ExtendsBase.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name: string;"));
        }
        
        assertTrue(generator.didWork());
    }

    private void setSettingsForTestJar() {
        m_settings.classFolder = "test_class";
        m_settings.packages.clear();
        m_settings.packages.add(TEST_JAR_PACAKGE);
        m_settings.classType = Settings.CLASS_TYPE;
    }
    
    /** This class has references to other classes */
    @Test
    public void testFamilyClassToInterface() throws Exception {
        m_settings.classType = Settings.INTERFACE_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Family.d.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name?: string;"));
            assertTrue(content + "\nShould contain dog field", content.contains("dog?: Dog;"));
            assertTrue(content + "\nShould contain dogs array field", content.contains("dogs?: Dog[];"));
            assertTrue(content + "\nShould contain dog import", content.startsWith("/// <reference path=\"Dog.d.ts\" />"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testFamilyClassToInterfaceWithPrefixedNames() throws Exception {
        m_settings.classType = Settings.INTERFACE_TYPE;
        m_settings.fieldPrefix = "m_";
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Family.d.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name?: string;"));
            assertTrue(content + "\nShould contain dog field", content.contains("dog?: Dog;"));
            assertTrue(content + "\nShould contain dogs array field", content.contains("dogs?: Dog[];"));
            assertFalse(content + "\nShould not contain the prefix name field", content.contains("m_name?: string;"));
            assertFalse(content + "\nShould not contain the prefix dog field", content.contains("m_dog?: Dog;"));
            assertFalse(content + "\nShould not contain the prefix dogs array field", content.contains("m_dogs?: Dog[];"));
            assertTrue(content + "\nShould contain dog import", content.startsWith("/// <reference path=\"Dog.d.ts\" />"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testFamilyClassToInterfaceWithMappingDogToCat() throws Exception {
        m_settings.classType = Settings.INTERFACE_TYPE;
        m_settings.mapping.put("Dog", "Cat");
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Dog.d.ts");
        assertFalse(outputFile + " Dog should not be generated: ", outputFile.exists());
        outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Cat.d.ts");
        assertTrue(outputFile + " Cat should be generated: ", outputFile.exists());
        outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Family.d.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name?: string;"));
            assertTrue(content + "\nShould contain dog field as cat", content.contains("dog?: Cat;"));
            assertTrue(content + "\nShould contain dogs array field as cat array", content.contains("dogs?: Cat[];"));
            assertTrue(content + "\nShould contain dog import", content.startsWith("/// <reference path=\"Cat.d.ts\" />"));
        }
        
        assertTrue(generator.didWork());
    }

    /** This class has references to other classes */
    @Test
    public void testFamilyClassToClass() throws Exception {
        m_settings.classType = Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Family.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name: string;"));
            assertTrue(content + "\nShould contain dog field", content.contains("dog: Dog;"));
            assertTrue(content + "\nShould contain dogs array field", content.contains("dogs: Dog[];"));
            assertTrue(content + "\nShould contain dog import", content.startsWith("import Dog = require('testResult/org.amdatu.typescriptgenerator.test.api/Dog')"));
        }
        
        assertTrue(generator.didWork());
    }

    /** This class has references to other classes */
    @Test
    public void testDepClassToClass() throws Exception {
        // First generate dependency
        m_settings.packages.clear();
        m_settings.packages.add(TEST_DEP_API);
        m_settings.classType = Settings.INTERFACE_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_DEP_API + "/Dep.d.ts");
        assertTrue(outputFile + " dep should be generated: ", outputFile.exists());
        
        m_settings.packages.clear();
        m_settings.clearOutput = false;
        m_settings.packages.add(TEST_PACAKGE);
        m_settings.classType = Settings.INTERFACE_TYPE;
        generator = new TypeScriptGenerator(m_settings);
        
        outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/WithDep.d.ts");
        assertTrue(outputFile + " dep should be generated: ", outputFile.exists());
        
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain dog import", content.startsWith("/// <reference path=\"../"+TEST_DEP_API+"/Dep.d.ts\" />"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testClassBoth() throws Exception {
        m_settings.classType = Settings.INTERFACE_TYPE + ", " + Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Animal.ts");
        assertTrue(outputFile + " class should be generated: ", outputFile.exists());
        
        outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Animal.d.ts");
        assertTrue(outputFile + " interface should be generated: ", outputFile.exists());

        outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Dog.d.ts");
        assertTrue(outputFile + " interface should be generated: ", outputFile.exists());
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testInnerClass() throws Exception {
        m_settings.classType = Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/Inner.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain name field", content.contains("name: string;"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testWithReferenceToInnerClass() throws Exception {
        m_settings.classType = Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/WithReferenceToInner.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain inner field", content.contains("inner: Inner;"));
        }
        
        assertTrue(generator.didWork());
    }

    @Test
    public void testComplexMaps() throws Exception {
        m_settings.classType = Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(m_settings);
        
        File outputFile = new File(m_settings.output + "/" + TEST_PACAKGE + "/WithComplexMaps.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain strings field", content.contains("strings: { [s: string]: Dog[]; };"));
            assertTrue(content + "\nShould contain unknown field", content.contains("unknown: { [s: string]: any; };"));
        }
        
        assertTrue(generator.didWork());
    }
}
