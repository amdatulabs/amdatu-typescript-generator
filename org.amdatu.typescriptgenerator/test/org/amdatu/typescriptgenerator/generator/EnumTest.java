package org.amdatu.typescriptgenerator.generator;

import static org.amdatu.typescriptgenerator.generator.TestHelper.TEST_PACAKGE;
import static org.amdatu.typescriptgenerator.generator.TestHelper.createTestSettings;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.util.Scanner;

import org.junit.Test;

public class EnumTest {

    @Test
    public void testEnumClass() throws Exception {
        Settings settings = createTestSettings(); 
        TypeScriptGenerator generator = new TypeScriptGenerator(settings);
        
        File outputFile = new File(settings.output + "/" + TEST_PACAKGE + "/ABC.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertFalse(content + "\nShould not contain a declare name", content.contains("declare"));
            assertTrue(content + "\nShould contain enum name", content.contains("enum ABC {"));
            assertTrue(content + "\nShould contain the export", content.contains("export = ABC"));
        }
        
        assertTrue(generator.didWork());
    }

    @Test
    public void testEnumInterface() throws Exception {
        Settings settings = createTestSettings();
        settings.enumType = Settings.INTERFACE_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(settings);
        
        File outputFile = new File(settings.output + "/" + TEST_PACAKGE + "/ABC.d.ts");
        assertTrue(outputFile + " should be generated: ", outputFile.exists());
        try (Scanner scanner = new Scanner(outputFile)) {
            String content = scanner.useDelimiter("\\Z").next();
            assertTrue(content + "\nShould contain a declare name", content.contains("declare"));
            assertTrue(content + "\nShould contain enum name", content.contains("enum ABC {"));
            assertTrue(content + "\nShould contain the export", content.contains("export = ABC"));
        }
        
        assertTrue(generator.didWork());
    }
    
    @Test
    public void testEnumBoth() throws Exception {
        Settings settings = createTestSettings();
        settings.enumType = Settings.INTERFACE_TYPE + ", " + Settings.CLASS_TYPE;
        TypeScriptGenerator generator = new TypeScriptGenerator(settings);
        
        File outputFile = new File(settings.output + "/" + TEST_PACAKGE + "/ABC.d.ts");
        assertTrue(outputFile + " class should be generated: ", outputFile.exists());
        
        outputFile = new File(settings.output + "/" + TEST_PACAKGE + "/ABC.d.ts");
        assertTrue(outputFile + " interface should be generated: ", outputFile.exists());
        
        assertTrue(generator.didWork());
    }

}
