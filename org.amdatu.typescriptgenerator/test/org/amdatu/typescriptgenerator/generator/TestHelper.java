package org.amdatu.typescriptgenerator.generator;

import java.io.File;

public class TestHelper {
    
    public static final String TEST_PACAKGE = "org.amdatu.typescriptgenerator.test.api";
    public static final String TEST_JAR_PACAKGE = "org.amdatu.typescriptgenerator.testjar";

    public static Settings createTestSettings() {
        Settings settings = new Settings();
        settings.classFolder = "build/classes/test";
        settings.clearOutput = true;
        settings.folder = new File("").getAbsolutePath();
        settings.packages.add(TEST_PACAKGE);
        settings.output = settings.folder + "/build/testResult";
        settings.baseFolder = settings.folder + "/build";
        return settings;
    }

}
