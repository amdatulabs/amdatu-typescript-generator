package org.amdatu.typescriptgenerator.model;

public class ClassPackageHolder {
    private final String className;
    private final String packageName;

    public ClassPackageHolder(String packageName, String className) {
        super();
        this.packageName = packageName;
        this.className = className;
    }

    public String getClassName() {
        return className;
    }

    public String getPackageName() {
        return packageName;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((className == null) ? 0 : className.hashCode());
        result = prime * result + ((packageName == null) ? 0 : packageName.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ClassPackageHolder other = (ClassPackageHolder) obj;
        if (className == null) {
            if (other.className != null)
                return false;
        }
        else if (!className.equals(other.className))
            return false;
        if (packageName == null) {
            if (other.packageName != null)
                return false;
        }
        else if (!packageName.equals(other.packageName))
            return false;
        return true;
    }

    @Override
    public String toString() {
        return "ClassPackageHolder [className=" + className + ", packageName=" + packageName + "]";
    }
    
}
