package org.amdatu.typescriptgenerator.generator;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.Temporal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.amdatu.typescriptgenerator.util.FileUtil;

public class TypeScriptGenerator {

    private final Map<String, String> classMapping = new HashMap<>();

    private boolean didWork = false;
    private final Settings m_settings;
    private ClassLoader m_classLoader;

    TypeScriptGenerator(Settings settings) throws Exception {
        classMapping.put("boolean", "boolean");
        classMapping.put("int", "number");
        classMapping.put("long", "number");
        classMapping.put("byte", "number");
        classMapping.put("short", "number");
        classMapping.put("float", "number");
        classMapping.put("double", "number");
        classMapping.put("char", "string");
        classMapping.put(Boolean.class.getName(), "boolean");
        classMapping.put(String.class.getName(), "string");
        classMapping.put(Number.class.getName(), "number");
        classMapping.put(Temporal.class.getName(), "string");
        classMapping.put(Date.class.getName(), "Date");
        classMapping.put(LocalDate.class.getName(), "Date");
        classMapping.put(LocalDateTime.class.getName(), "Date");
        classMapping.put(Instant.class.getName(), "Date");
        
        m_settings = settings;
        
        classMapping.putAll(settings.types);
        startGenerator();
    }
    
    private void startGenerator() throws Exception {
        if (m_settings.clearOutput) {
            FileUtil.deleteTypeScriptFiles(new File(m_settings.output));
        }
        createClassPath();
        
        Files.walk(Paths.get(m_settings.folder))
        .filter((f) -> f.toString().endsWith(".class"))
        .forEach(this::processFile);
    }

    private void createClassPath() throws IOException {
        LinkedHashSet<URL> classPaths = new LinkedHashSet<>(); //Use LinkedHashSet to maintain classPathEntry order
        Files.walk(Paths.get(m_settings.folder))
            .filter((f) -> f.toString().endsWith(".class"))
            .forEach((f) -> createClassLoader(f, classPaths));
        
        for (String extraPath : m_settings.classPath) {
            if (extraPath.endsWith(".jar") || extraPath.endsWith(".class")) {
                classPaths.add(new File(extraPath).toURI().toURL());
            } else {
                // Process files
                Files.walk(Paths.get(extraPath))
                    .filter((f) -> f.toString().endsWith(".class") || f.toString().endsWith(".jar"))
                    .forEach((f) -> {
                        try {
                            classPaths.add(f.toUri().toURL());
                        }
                        catch (Exception e) {
                            throw new RuntimeException(e);
                        }
                    });
            }
        }
        
        m_classLoader = new URLClassLoader(classPaths.toArray(new URL[0]));
    }

    private void createClassLoader(Path classFile, Set<URL> classPaths) {
        String fileString = classFile.toString();
        if (isPackageNameIncluded(fileString, m_settings.excludePattern)) {
            return;
        }
        String binFolder = File.separator + m_settings.classFolder + File.separator;
        if (!fileString.contains(binFolder)) {
            // File is not in the class folder
            return;
        }
        String beginPath = fileString.substring(0, fileString.lastIndexOf(binFolder) + binFolder.length());
        String packageName =
            fileString.substring(fileString.lastIndexOf(binFolder) + binFolder.length(),
                fileString.lastIndexOf(File.separator)).replace(File.separator, ".");

        if (!isIncluded(packageName)) {
            // Skip file
            return;
        }
        try {
            classPaths.add(new File(beginPath).toURI().toURL());
        }
        catch (MalformedURLException e) {
        }
    }

    private void processFile(Path classFile) {
        try {
            String fileString = classFile.toString();
            if (isPackageNameIncluded(fileString, m_settings.excludePattern)) {
                return;
            }
            String binFolder = File.separator + m_settings.classFolder + File.separator;
            String packageName =
                fileString.substring(fileString.lastIndexOf(binFolder) + binFolder.length(),
                    fileString.lastIndexOf(File.separator)).replace(File.separator, ".");

            if (!isIncluded(packageName) || !fileString.contains(binFolder)) {
                // Skip file, as it's not the correct package or the bin folder is not correct
                return;
            }
            String fileName = classFile.getFileName().toString();
            String className = packageName + "." + fileName.substring(0, fileName.lastIndexOf("."));
            if (isPackageNameIncluded(className, m_settings.excludePattern)) {
                return;
            }
            System.out.println("Processing: " + className);

            Class<?> cls = m_classLoader.loadClass(className);
            if (cls.isAnnotation()) {
                System.out.println("  Skipping annotation");
            } else if (cls.isInterface()) {
                System.out.println("  Skipping interface");
            } if(cls.isEnum()) {
                new EnumWriter(m_settings, cls).write();
                didWork = true;
            } else if (Class.forName("java.lang.Object").isAssignableFrom(cls)) {
                try {
                    new ClassWriter(m_settings, cls, this).write();
                    didWork = true;
                } catch (LinkageError e) {
                    System.err.println("  Unable to process class due to: " + e.getClass().getName() + ": " + e.getMessage());
                }
            } else {
                System.out.println("  Skipping unknown type");
            }
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        catch (ClassNotFoundException | LinkageError e) {
            System.out.println("  Can't load class: " + e.getMessage());
        }
    }

    private boolean isIncluded(String packageName) {
        if (m_settings.packages.isEmpty() || isPackageNameIncluded(packageName, m_settings.packages)) {
            if (!isPackageNameIncluded(packageName, m_settings.excludePackages)) {
                return true;
            }
        }
        return false;
    }

    private boolean isPackageNameIncluded(String packageName, List<String> packages) {
        for (String pack : packages) {
            if (packageName.matches(pack)) {
                return true;
            }
        }
        return false;
    }

    public boolean didWork() {
        return didWork;
    }

    public Map<String, String> getClassMapping() {
        return classMapping;
    }

    public ClassLoader getClassLoader() {
        return m_classLoader;
    }

}
