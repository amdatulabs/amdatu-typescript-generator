package org.amdatu.typescriptgenerator.generator;

import java.io.File;
import java.util.Map.Entry;

public abstract class AbstractWriter {

    private static final String DO_NOT_MODIFY_HEADER = "// AUTO-GENERATED FILE, DO NOT MODIFY!\n\n";

    protected StringBuilder body = new StringBuilder();
    protected StringBuilder info = new StringBuilder();
    protected Settings m_settings;
    protected Class<?> m_cls;
    protected String m_outputFolder;
    
    public AbstractWriter(Settings settings, Class<?> cls) {
        m_settings = settings;
        m_cls = cls;
        
        m_outputFolder = m_settings.output + File.separator + m_cls.getPackage().getName() + File.separator;
        body.append(DO_NOT_MODIFY_HEADER);
        new File(m_outputFolder).mkdirs();
    }
    
    protected String getNormalizedClassName(String className) {
        String simpleClassName = className;
        if (className.contains("$")) {
            simpleClassName = className.substring(className.lastIndexOf("$") + 1);
        }
        for (Entry<String, String> e : m_settings.mapping.entrySet()) {
            if (simpleClassName.equals(e.getKey())) {
                return e.getValue();
            }
        }
        return simpleClassName;
    }
    
    protected String getFileName(String type) {
        String fileName = getNormalizedClassName(m_cls.getSimpleName());
        if (Settings.INTERFACE_TYPE.equals(type)) {
            fileName += ".d";
        } 
        fileName += ".ts";
        return fileName;
    }
  
}
