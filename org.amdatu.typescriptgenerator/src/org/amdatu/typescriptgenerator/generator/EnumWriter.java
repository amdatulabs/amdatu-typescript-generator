package org.amdatu.typescriptgenerator.generator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;

public class EnumWriter extends AbstractWriter {
    
    public EnumWriter(Settings settings, Class<?> cls) {
        super(settings, cls);
    }
    
    public void write() throws IOException {
        for (String type : m_settings.enumType.split(",")) {
            writeType(type.trim());
        }
        System.out.println(info.toString());
    }
    
    private void writeType(String type) throws IOException {
        // Only need the info once 
        info.setLength(0);
        if (Settings.INTERFACE_TYPE.equals(type)) {
            // Interface needs a declare for enums
            body.append("declare ");
        }
        body.append("enum " + getNormalizedClassName(m_cls.getSimpleName()) + " {\n");
        boolean first = true;
        int ordinal = 0;
        for (Object attr : m_cls.getEnumConstants()) {
            info.append("  " + attr.toString() + "\n");
            if (!first) {
                body.append(",\n");
            }
            body.append("    " + attr.toString());
            if (Settings.INTERFACE_TYPE.equals(type)) {
                body.append(" = ");
                body.append(ordinal);
            } // no else, class does not need anything
            ordinal++;
            first = false;
        }
        body.append("\n}");
        body.append("\n\nexport = " + getNormalizedClassName(m_cls.getSimpleName()) + ";\n");

        PrintWriter out = new PrintWriter(new File(m_outputFolder + getFileName(type)));
        out.write(body.toString());
        out.close();
    }

}
