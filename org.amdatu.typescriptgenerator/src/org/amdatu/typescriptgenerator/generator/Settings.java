package org.amdatu.typescriptgenerator.generator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Settings {

    public static final String CLASS_TYPE = "class";
    public static final String INTERFACE_TYPE = "interface";
    
    /** The folder to process */
    public String folder = "";
    /** The (sub) folder in this folder that contains the class files */
    public String classFolder = "bin";
    /** Write to where? */
    public String output = "typescript/definitions";
    public String baseFolder = "";
    /** Indicates if fields are prefixed with something like m_file */
    public String fieldPrefix = "";
    /** Do we need to clear the output */
    public boolean clearOutput;
    /** Packages to process (regex) */
    public List<String> packages = new ArrayList<>();
    /** Packages to exclude from the process (regex) */
    public List<String> excludePackages = new ArrayList<>();
    /** Files to exclude */
    public List<String> excludePattern = new ArrayList<>();
    /** Mapping between types int => number */
    public Map<String, String> types = new HashMap<>();
    /** New names for classes */
    public Map<String, String> mapping = new HashMap<>();
    /** export type for enums */
    public String enumType = CLASS_TYPE;
    /** export type for classes */
    public String classType = INTERFACE_TYPE;
    /** extra class path */
    public List<String> classPath = new ArrayList<>();
}
