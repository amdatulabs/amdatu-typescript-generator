package org.amdatu.typescriptgenerator.generator;

import java.io.File;
import java.io.FileInputStream;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

public class TypeScriptGeneratorLoader {

    public static final String TYPESCRIPT_SETTINGS = "typescript.settings.json";

    public TypeScriptGenerator init(File baseDir) throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        File settingsFile = new File(baseDir, TYPESCRIPT_SETTINGS);
        if (settingsFile.exists()) {
            Settings settings = objectMapper.readValue(new FileInputStream(settingsFile), Settings.class);
            if (settings.folder != null && !"".equals(settings.folder.trim())) {
                return new TypeScriptGenerator(settings);
            }
        }
        System.out.println("TypeScript Generator skipped, as there is no settings file in: " + baseDir.getAbsolutePath());
        return null;
    }

}
