package org.amdatu.typescriptgenerator.generator;

import java.io.File;
import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.Map.Entry;
import java.util.Set;

import org.amdatu.typescriptgenerator.model.ClassPackageHolder;

import sun.reflect.generics.reflectiveObjects.ParameterizedTypeImpl;

public class ClassWriter extends AbstractWriter {
    
    private final TypeScriptGenerator m_generator;
    private final LinkedHashMap<String, String> m_classAttrs = new LinkedHashMap<>();
    private final Set<ClassPackageHolder> m_imports = new HashSet<>();
    
    
    public ClassWriter(Settings settings, Class<?> cls, TypeScriptGenerator generator) {
        super(settings, cls);
        this.m_generator = generator;
    }

    private void getFields(Class<?> cls, TypeScriptGenerator generator, String outputType) throws ClassNotFoundException {
        Class<?> superClass = cls.getSuperclass();
        if (superClass != null) {
            getFields(superClass, generator, outputType);
        }
        String fieldPrefix = null;
        if (m_settings.fieldPrefix != null && m_settings.fieldPrefix.trim().length() > 0) {
            fieldPrefix = m_settings.fieldPrefix.trim();
        }
        for (java.lang.reflect.Field f : cls.getDeclaredFields()) {
            String typescriptClass = getTypescriptClass(f.getType(), f.getGenericType(), outputType);
            String fieldName = f.getName();
            if (fieldPrefix != null && fieldName.startsWith(fieldPrefix)) {
                fieldName = fieldName.substring(fieldPrefix.length());
            }
            info.append(" Found field: " + fieldName + " : " + f.getType().getName() + " => "
                + typescriptClass + "\n");
            m_classAttrs.put(fieldName, typescriptClass);
        }
    }
    
    public void write() throws IOException, ClassNotFoundException {
        for (String type : m_settings.classType.split(",")) {
            m_classAttrs.clear();
            m_imports.clear();
            getFields(m_cls, m_generator, type);
            writeType(type.trim());
        }
        System.out.print(info.toString());
    }
    
    private void writeType(String type) throws IOException {
        if (Settings.INTERFACE_TYPE.equals(type)) {
            body.append("interface ");
        } else {
            body.append("class ");
        }
        String normalizedClassName = getNormalizedClassName(m_cls.getSimpleName());
        if (normalizedClassName == null || "".equals(normalizedClassName)) {
            // Can be an inner class for example, we don't process them
            return;
        }
        body.append(normalizedClassName + " {\n");
        for (Entry<String, String> attr : m_classAttrs.entrySet()) {
            body.append("    " + attr.getKey());
            if (Settings.INTERFACE_TYPE.equals(type)) {
                // optional only allowed in interface
                body.append("?");
            }
            body.append(": " + attr.getValue() + ";\n");
        }
        body.append("}");
        if (Settings.CLASS_TYPE.equals(type)) {
            // Class needs an export
            body.append("\n\nexport = " + getNormalizedClassName(m_cls.getSimpleName()) + ";\n");
        }

        PrintWriter out = new PrintWriter(new File(m_outputFolder + getFileName(type)));
        out.write(generateImports(type));
        out.write(body.toString());
        out.close();
    }
    
    private String generateImports(String type) {
        StringBuilder imports = new StringBuilder();
        for (ClassPackageHolder importClass : m_imports) {
            if (m_cls.getName().equals(importClass.getPackageName() + "." + importClass.getClassName())) {
                // Don't create self references
                continue;
            }
            if (Settings.INTERFACE_TYPE.equals(type)) {
                imports.append("/// <reference path=\"");
                if (!m_cls.getPackage().getName().equals(importClass.getPackageName())) {
                    // Not the same package so we need to correctly import that
                    imports.append("../" + importClass.getPackageName() + "/");
                }
                imports.append(importClass.getClassName() + ".d.ts\" />\n");
            } else {
                int startIndex = m_settings.output.lastIndexOf(m_settings.baseFolder);
                if ("".equals(m_settings.baseFolder)) {
                    startIndex = 0;
                }
                String folder = m_settings.output.substring(startIndex + m_settings.baseFolder.length());
                // Remove first / if needed
                if (folder.startsWith("/")) {
                    folder = folder.substring(1, folder.length());
                }
                // add last / if needed
                if (!folder.endsWith("/")) {
                    folder += "/";
                }
                imports.append("import " + importClass.getClassName() + " = require('" + folder + importClass.getPackageName() + "/" + importClass.getClassName() + "')\n");
            }
        }
        imports.append("\n");
        return imports.toString();
    }

    public String getTypescriptClass(final Class<?> clazzParam, Type type, String outputType) throws ClassNotFoundException {
        Class<?> clazz = clazzParam;
        String className = clazz.getName();
        // First check for an exact match:
        for (Entry<String, String> e : m_generator.getClassMapping().entrySet()) {
            if (e.getKey().equals(className)) {
                return e.getValue();
            }
        }
        String typeFormat = "%s";
        if (m_generator.getClassLoader().loadClass("java.util.Collection").isAssignableFrom(m_generator.getClassLoader().loadClass(clazz.getName()))) {
            ParameterizedType stringListType = (ParameterizedType) type;
            clazz = (Class<?>) stringListType.getActualTypeArguments()[0];
            className = clazz.getName();
            typeFormat = "%s[]";
        }
        
        if (m_generator.getClassLoader().loadClass(java.util.Map.class.getName()).isAssignableFrom(m_generator.getClassLoader().loadClass(clazz.getName()))) {
            ParameterizedType stringListType = (ParameterizedType) type;
            if (((Class<?>) stringListType.getActualTypeArguments()[0]).getName().equals(String.class.getName())){
                if (stringListType.getActualTypeArguments()[1] instanceof ParameterizedTypeImpl) {
                    ParameterizedTypeImpl paramType = (ParameterizedTypeImpl) stringListType.getActualTypeArguments()[1];
                    if (m_generator.getClassLoader().loadClass("java.util.Collection").isAssignableFrom(m_generator.getClassLoader().loadClass(paramType.getRawType().getName()))) {
                        if (paramType.getActualTypeArguments()[0] instanceof Class) {
                            clazz = (Class<?>) paramType.getActualTypeArguments()[0];
                            className = clazz.getName();
                            typeFormat = "{ [s: string]: %s[]; }";
                        } else {
                            // to deep of a nesting, its a map with a list of a list etc...
                            return "{ [s: string]: any; }";
                        }
                    } else {
                        return "any";
                    }
                } else if (stringListType.getActualTypeArguments()[1] instanceof Class) {
                    clazz = (Class<?>) stringListType.getActualTypeArguments()[1];
                    className = clazz.getName();
                    typeFormat = "{ [s: string]: %s; }";
                }
            } else {
                return "any";
            } 
        }
        
        for (Entry<String, String> e : m_generator.getClassMapping().entrySet()) {
            try {
                if (m_generator.getClassLoader().loadClass(e.getKey()).isAssignableFrom(m_generator.getClassLoader().loadClass(className))) {
                    return String.format(typeFormat, e.getValue());
                }
            } catch (LinkageError | ClassNotFoundException le) {
                // Ignore this one, can happen with primitives
            }
        }
        
        if (clazz.isEnum() && Settings.INTERFACE_TYPE.equals(outputType)) {
            // interfaces can't have imports to enums
            return String.format(typeFormat, "any"); 
        }

        String classPackage = className.substring(0, className.lastIndexOf("."));
        String simpleClassName = getNormalizedClassName(className.substring(className.lastIndexOf(".") + 1));
        if (m_cls.getPackage().getName().equals(classPackage) || doesDefinitionExist(classPackage, simpleClassName, outputType)) {
            // Same package we can import it
            m_imports.add(new ClassPackageHolder(classPackage, simpleClassName));
            return String.format(typeFormat, simpleClassName);
        }
        
        System.out.println("  No mapping for: " + clazz.getName());
        return String.format(typeFormat, "any");
    }

    private boolean doesDefinitionExist(String classPackage, String simpleClassName, String outputType) {
        String extention = Settings.INTERFACE_TYPE.equals(outputType)? ".d.ts" : ".ts";
        return new File(m_settings.output + File.separator + classPackage + File.separator + getNormalizedClassName(simpleClassName) + extention).exists();
    }


}
