package org.amdatu.typescriptgenerator.util;

import java.io.File;
import java.io.IOException;

public final class FileUtil {
    
    private FileUtil() {
        // private it's a util
    }

    /** This will remove all typescript files, and will leave other files intact */
    public static void deleteTypeScriptFiles(File f) throws IOException {
        if (f.isDirectory()) {
            for (File c : f.listFiles()) {
                deleteTypeScriptFiles(c);
            }
        }
        if (f.isDirectory() || (f.isFile() && f.getName().endsWith(".ts"))) {
            f.delete();
        } else {
            System.out.println("Not deleting: " + f.getAbsolutePath() + " as it's not a TypeScript file, are you sure the output folder is set correctly?");
        }
    }
}
