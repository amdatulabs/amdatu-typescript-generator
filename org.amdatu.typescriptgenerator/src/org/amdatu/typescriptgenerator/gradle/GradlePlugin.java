package org.amdatu.typescriptgenerator.gradle;

import org.gradle.api.InvalidUserDataException;
import org.gradle.api.Project;
import org.gradle.api.Plugin;

public class GradlePlugin implements Plugin<Project> {

    @Override
    public void apply(Project target) {
        try {
            target.getTasks().create("generateTypeScript", GenerateTask.class).dependsOn(target.getTasksByName("compileJava", false));
        }
        catch (InvalidUserDataException e) {
            throw new RuntimeException(e);
        }
    }

}
