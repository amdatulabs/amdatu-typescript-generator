package org.amdatu.typescriptgenerator.gradle;

import org.amdatu.typescriptgenerator.generator.TypeScriptGenerator;
import org.amdatu.typescriptgenerator.generator.TypeScriptGeneratorLoader;
import org.gradle.api.DefaultTask;
import org.gradle.api.tasks.TaskAction;

public class GenerateTask extends DefaultTask {

    @TaskAction
    public void generateTypeScript() throws Exception {
        TypeScriptGeneratorLoader loader = new TypeScriptGeneratorLoader();
        TypeScriptGenerator generator = loader.init(getProject().getProjectDir());
        if (generator != null) {
            this.setDidWork(generator.didWork());
        } else {
            // No generator, did nothing
            this.setDidWork(false);
        }
    }

}
