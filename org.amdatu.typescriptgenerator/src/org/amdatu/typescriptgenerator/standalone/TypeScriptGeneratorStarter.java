package org.amdatu.typescriptgenerator.standalone;

import java.io.File;
import java.io.IOException;

import org.amdatu.typescriptgenerator.generator.Settings;
import org.amdatu.typescriptgenerator.generator.TypeScriptGenerator;
import org.amdatu.typescriptgenerator.generator.TypeScriptGeneratorLoader;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

public class TypeScriptGeneratorStarter {
    
    public static void main(String[] args) throws Exception {
        TypeScriptGeneratorLoader loader = new TypeScriptGeneratorLoader();
        
        File baseDir;
        if (args.length == 1) {
            baseDir = new File(args[0]);
        } else {
            baseDir = new File(".");
        } 
        
        TypeScriptGenerator generator = loader.init(baseDir);
        if (generator == null) {
            noSettings();
        }
    }

    private static void noSettings() throws IOException {
        Settings settings = new Settings();
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        System.out.println("Please create a generator file: " + TypeScriptGeneratorLoader.TYPESCRIPT_SETTINGS);
        System.out.println("With the following contents: (for example)");
        
        settings.folder = "./";
        settings.packages.add(".*\\.api");
        settings.excludePackages.add(".*\\.generated\\.api");
        settings.types.put("com.example.clazz", "string");
        
        System.out.println(objectMapper.writeValueAsString(settings));
    }
}
