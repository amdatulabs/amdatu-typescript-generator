# Amdatu TypeScript Generator

Amdatu TypeScript Generator creates TypeScript classes and/or interfaces from your Java class or enum files.

## Usage
The generator can be started in Gradle or standalone from the command-line.
In any case build the project first. Just execute `./gradlew` and a Gradle build will start

### Gradle
Add the following configuration to the `gradle.build` file to use the generator as a Gradle plugin:
```gradle
buildscript {
    repositories {
        maven {
            url uri('location/of/repo')
        }
    }
    dependencies {
        classpath group: 'org.amdatu',
                   name: 'org.amdatu.typescriptgenerator',
                version: '1.0-SNAPSHOT'
    }
}

apply plugin: 'typescript-gen-plugin'
```
and change the location/of/repo to the repo folder generated in the build (git-repo-of-amdatu-typescript-generator/repo).

Once this has been setup, you can call 
```
gradle generateTypeScript
``` 
on your project and the plugin will start.

__Sub projects__

If you have sub projects and will have a *generator configuration file* per project you might want to move the `apply plugin: 'typescript-gen-plugin'` in the `gradle.build` file to be under the subproject configuration. Otherwise it will only process the *generator configuration file* in the root project.

### Standalone
The jar file (*TypeScriptGeneratorPlugin-*.jar*) genereated in the repo or in `build/libs/` can just be executed with a *java -jar* command. For example: 
```
java -jar build/libs/TypeScriptGeneratorPlugin-1.0-SNAPSHOT.jar
```

# Generator configuration file
In order for the generator to know what to generate a configuration file is needed.
This file should be named: `typescript.settings.json`

And contain the following:
```json
{
  "folder" : "./",
  "classFolder" : "bin",
  "output" : "typescript/definitions",
  "baseFolder" : "",
  "clearOutput" : false,
  "packages" : [ ".*\\.api" ],
  "excludePackages" : [ ".*\\.generated\\.api" ],
  "excludePattern" : [ ],
  "types" : {
    "com.example.clazz" : "string"
  },
  "mapping" : { 
    "Dog" : "Cat"
  },
  "enumType" : "class",
  "classType" : "interface",
  "classPath" : [ ]
}
```
### folder
**type**: `string`  
This is the folder to get the class files from, usually the project root folder. Please note that for a Gradle build the working directory is the folder where the build file is located.

### classFolder
**type**: `string`   
**default**: `bin`   
In the `folder` directory there should be a directory with the class files, usually `bin` or `target/classes`.

### output
**type**: `string`  
**default**: `typescript/definitions`   
Define the folder where the TypeScript files are written.

### baseFolder
**type**: `string`  
Where is the application root folder? This is used to generate the imports in TypeScript. If the output is `typescript/definitions` and the `baseFolder` is `typescript` then the imports will be like: `definitions/package.api/SomeClass.ts`

### clearOutput
**type**: `boolean`  
**default**: `false`   
Indicates to remove all TypeScript files in the output folder. Note that this will not delete any other files.

### packages
**type**: `regex string`  
Define what packages to process. Note that the string is a regex. so a `.` will match all chars, so if you want a actual dot you will need `\\.` as it's a Java string.

### excludePackages
**type**: `regex string`  
Define what packages not to process, useful if the package defined with `packages` contains or matches more folders. Note that the string is a regex. so a `.` will match all chars, so if you want a actual dot you will need `\\.` as it's a Java string.

### excludePattern
**type**: `regex string`    
Define what files not to process. For example stop the processing of builders: `.*Builder`

### mapping
**type**: `map<string, string>`   
Add some custom mapping for example: `"com.example.clazz" : "string"`, this will map the Java class `com.example.clazz` to a TypeScript `string`

### types
**type**: `map<string, string>`   
Rename classes, for example: `"Dog" : "Cat"` will rename the java `Dog` class to a TypeScript `Cat` class

### enumType
**type**: `string`  
**default**: `class`    
The output format for enums. This can be `class` or `interface` or if you want both `class,interface`

### classType
**type**: `string`  
**default**: `interface`    
The output format for classes. This can be `class` or `interface` or if you want both `class,interface`

### classPath
**type**: `list<string>`  
Add extra .class or .jar files to the classpath, all subfolders will be scanned for .class and .jar files to add to the classpath.

## Developing
The project consists of 2 eclipse projects:      
  - __org.amdatu.typescriptgenerator__ - The main project       
  - __org.amdatu.typescriptgenerator.testjar__ - Project that hosts extra classes that need to be loaded in a unit test, to test class loading of the generator

## Links

* [Amdatu Website]
* [Source Code]
* [Issue Tracking]
* [Continuous Build]

## License

The Amdatu Web project is licensed under [Apache License 2.0](http://www.apache.org/licenses/LICENSE-2.0.html).